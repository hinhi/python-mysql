import sys
from contextlib import contextmanager
from datetime import datetime
import uuid
import random
import time

from sqlalchemy import MetaData
from sqlalchemy import Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker


CONF = {
    'user': 'driver_test',
    'pass': 'driver_test',
    'host': '172.17.0.1',
    'port': '3306',
    'db': 'driver_test'
}
urls = {
    'mysqldb': 'mysql+mysqldb://{user}:{pass}@{host}:{port}/{db}?charset=utf8mb4',
    'pymysql': 'mysql+pymysql://{user}:{pass}@{host}:{port}/{db}?charset=utf8mb4',
    'cymysql': 'mysql+cymysql://{user}:{pass}@{host}:{port}/{db}?charset=utf8',
    'mysqlconnector': 'mysql+mysqlconnector://{user}:{pass}@{host}:{port}/{db}?charset=utf8mb4',
}

engine = urls[sys.argv[1]].format(**CONF)
session = scoped_session(
    sessionmaker(
        autocommit=False,
        autoflush=True,
    )
)
metadata = MetaData(bind=engine)
Base = declarative_base(bind=engine)


_str = 'アイウエオカキクケコさしすせそたちつてと'


class TestTable(Base):
    __table__ = Table('big_table', metadata, autoload=True)
    @classmethod
    def create(cls):
        self = cls()
        for i in range(5):
            setattr(self, 'datetime%i' % i, datetime.now())
        for i in range(5):
            setattr(self, 'guid%i' % i, str(uuid.uuid4()))
        for i in range(10):
            setattr(self, 'int%i' % i, random.randint(0, 0xFFFFFF))
            setattr(self, 'int%2.2i' % i, random.randint(0, 0xFFFFFF))
        for i in range(10):
            setattr(self, 'flag%i' % i, bool(random.randint(0, 1)))
        for i in range(10):
            setattr(self, 'char%i' % i, ''.join(random.choices(_str, k=random.randint(100, 200))))
            setattr(self, 'char%2.2i' % i, ''.join(random.choices(_str, k=random.randint(100, 200))))
        return self


def insert(entity_cls, n):
    for i in range(n):
        session.add(entity_cls.create())
    session.commit()


def fetch(entity_cls, n):
    return list(session.query(entity_cls)[:n])


def delete(entities):
    for entity in entities:
        session.delete(entity)
    session.commit()


@contextmanager
def timer(msg):
    t1 = time.time()
    yield
    print('  %s %.3f' % (msg, time.time() - t1))


def main():
    driver = sys.argv[1]
    action = sys.argv[2]
    n = int(sys.argv[3])
    print(driver, action, n)
    entity_cls = TestTable
    if action == 'insert':
        with timer('insert'):
            insert(entity_cls, n)
    elif action == 'fetch':
        with timer('fetch'):
            fetch(entity_cls, n)
    elif action == 'delete':
        with timer('fetch '):
            entities = fetch(entity_cls, n)
        with timer('delete'):
            delete(entities)


if __name__ == '__main__':
    main()
