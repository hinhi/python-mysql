#!/bin/bash -eu
PYTHON=python3
SCRIPT=./test.py

if [ $# = 0 ]; then
    n=10000
else
    n=$1
fi

$PYTHON $SCRIPT $DRIVER insert $n
$PYTHON $SCRIPT $DRIVER fetch $n
$PYTHON $SCRIPT $DRIVER delete $n
